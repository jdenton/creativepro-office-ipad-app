MenuView = {};
MenuView.window = Ti.UI.createWindow({
	top:   0,
	left:  0,
	width: 300,
	backgroundImage: 'images/bgSideMenu.png',
	backgroundRepeat: true
});

var menuItem0,menuItem1,menuItem2,menuItem3,menuItem4,menuItem5,menuItem6,menuItem7;

MenuView.init = function() {
	/*
	 * SEARCH BAR
	 */
	MenuView.searchBar = Ti.UI.createSearchBar({
	    barColor: '#000', 
	    showCancel: false,
	    height: 43,
	    hintText: 'Search your office',
	    top: 0
	});
	
	/*
	 * FOOTER BUTTON BAR
	 */
	var flexSpace = Ti.UI.createButton({ 
	    systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
	});	
	
	var btnDashboard = Ti.UI.createButton({
		top: 11,
		left: 40,
		width: 34,
		height: 34,
		backgroundImage: 'images/icon-bar-bottom-dashboard-2.png'
	});
	
	var btnRefresh = Ti.UI.createButton({
		top: 11,
		left: 100,
		width: 34,
		height: 34,
		backgroundImage: 'images/icon-bar-bottom-refresh-2.png'
	});
	
	var btnSettings = Ti.UI.createButton({
		top: 11,
		left: 160,
		width: 34,
		height: 34,
		backgroundImage: 'images/icon-bar-bottom-settings-2.png'
	});
	
	var btnAccount = Ti.UI.createButton({
		top: 11,
		left: 220,
		width: 34,
		height: 34,
		backgroundImage: 'images/icon-bar-bottom-account-2.png'
	});
	
	var toolBarView = Ti.UI.createView({
		left: 0,
		bottom: 0,
		height: 50,
		backgroundImage: 'images/bgMenuItemSelected.png'
	});
	toolBarView.add(btnDashboard,btnRefresh,btnSettings,btnAccount);
			
	/*
	 * MAIN MENU ITEMS
	 */	
	menuItem0 = Styles.menuLabel('Dashboard','dashboard.png',0,'menuItemDashboard');
	menuItem1 = Styles.menuLabel('Projects','projects.png',50,'menuItemProjects');
	menuItem2 = Styles.menuLabel('Tasks','tasks.png',100,'menuItemTasks');
	menuItem3 = Styles.menuLabel('Ledger','ledger.png',150,'menuItemLedger');
	menuItem4 = Styles.menuLabel('Invoices & Estimates','invoice.png',200,'menuItemInvoices');
	menuItem5 = Styles.menuLabel('Expenses','expenses.png',250,'menuItemExpenses');
	menuItem6 = Styles.menuLabel('Timesheets','timer.png',300,'menuItemTimesheets');
	menuItem7 = Styles.menuLabel('Reports','reports.png',350,'menuItemReports');
	
	MenuView.timerMenuItem = Ti.UI.createView({
		top: 350,
		left: 0,
		width: 300,
		height: 50,
		id: 'menuItemTimer',
		backgroundImage: 'images/bgMenuTimerOff.png'
	});
	
	MenuView.lblTimerTitle = Ti.UI.createLabel({
		text: 'Job Timer',
		font: { 
	    	fontSize: 20,
	    	fontWeight: 'bold',
	    	fontFamily: 'franklin'
	    },
	    width: 240,
	    top: 11,
	    left: 60,
	    color: '#fff',
	    shadowColor: '#0842a3',
	    shadowOffset:{ x: 1, y: 1 }
	});
	
	MenuView.lblTimerProject = Ti.UI.createLabel({
		text: '',
		font: { 
	    	fontSize: 13,
	    	fontWeight: 'bold',
	    	fontFamily: 'franklin'
	    },
	    width: 240,
	    top: 36,
	    left: 60,
	    color: '#000'
	});
	
	MenuView.lblTimerTask = Ti.UI.createLabel({
		text: '',
		font: { 
	    	fontSize: 13,
	    	fontWeight: 'bold',
	    	fontFamily: 'franklin'
	    },
	    width: 240,
	    top: 52,
	    left: 60,
	    color: '#000'
	});
	
	var icon = Ti.UI.createLabel({
		width: 30,
		height: 30,
		left: 10,
		backgroundImage: 'images/clock-green.png'
	});
	
	MenuView.timerMenuItem.add(
		icon,
		MenuView.lblTimerTitle,
		MenuView.lblTimerProject,
		MenuView.lblTimerTask
	);
	
	MenuView.viewSearchResults = Ti.UI.createView({
		visible: false,
		top: 43,
		width: 300
	});
	
	MenuView.viewMenu = Ti.UI.createView({
		width: 300,
		top: 43
	});
	MenuView.viewMenu.add(
		menuItem0,
		menuItem1,
		menuItem2,
		menuItem3,
		menuItem4,
		menuItem5,
		menuItem6,
		menuItem7,
		MenuView.timerMenuItem
	);
	
	MenuView.window.add(
		MenuView.searchBar,
		MenuView.viewMenu,
		MenuView.viewSearchResults,
		toolBarView
	);
	MenuView.window.open();
	
	/*
	 * ADD EVENT HANDLERS
	 */
	Ti.App.addEventListener('timerStart', function(e) {
		MenuView.lblTimerProject.text = e.values[0];
		MenuView.lblTimerTask.text = e.values[1];
		
		MenuView.timerMenuItem.animate({
			height: 76
		});
		
		MenuView.timerMenuItem.backgroundImage = 'images/bgMenuTimerOn.png';
		MenuView.lblTimerTitle.color = '#000';
		MenuView.lblTimerTitle.shadowColor = '#df7808';
	});
	
	Ti.App.addEventListener('timerClear', function(e) {
		MenuView.timerMenuItem.backgroundImage = 'images/bgMenuTimerOff.png';
		MenuView.lblTimerTitle.color = '#fff';
		MenuView.lblTimerTitle.shadowColor = '#0842a3';
		MenuView.lblTimerTitle.text = 'Job Timer';
		
		MenuView.lblTimerProject.text = '';
		MenuView.lblTimerTask.text = '';
		
		MenuView.timerMenuItem.animate({
			height: 50
		});
	});
	
	Ti.App.addEventListener('timerIncrement', function(e) {
		MenuView.lblTimerTitle.text = e.time;
	});	
	
	menuItem0.addEventListener('click', function() {
		MenuView.switchWindows('dashboard');
	});
	
	menuItem1.addEventListener('click', function() {
		MenuView.switchWindows('projects');
	});
	
	menuItem2.addEventListener('click', function() {
		MenuView.switchWindows('tasks');
	});
	
	menuItem3.addEventListener('click', function() {
		MenuView.switchWindows('ledger');
	});
	
	menuItem4.addEventListener('click', function() {
		MenuView.switchWindows('invoices');
	});
	
	menuItem5.addEventListener('click', function() {
		MenuView.switchWindows('expenses');
	});
	
	menuItem6.addEventListener('click', function() {
		MenuView.switchWindows('timesheets');
	});
	
	menuItem7.addEventListener('click', function() {
		MenuView.switchWindows('reports');
	});
	
	MenuView.timerMenuItem.addEventListener('click', function() {
		Timer.popup.show({ view: this });
	});
	
	/*
	 * Search bar events
	 */
	var lastSearch = null, timer;
	MenuView.searchBar.addEventListener('change', function() {
		if (MenuView.searchBar.value.length == 0) {
			MenuView.viewMenu.visible = true;
			MenuView.viewSearchResults.visible = false;
		} else {
			MenuView.viewMenu.visible = false;
			MenuView.viewSearchResults.visible = true;
			
			if (MenuView.searchBar.value.length > 2 && MenuView.searchBar.value !=  lastSearch) {			
				clearTimeout(timer);
			    timer = setTimeout(function() {
			    	lastSearch = MenuView.searchBar.value;
			    	DataSearch.getSearchBarResults(MenuView.searchBar.value);
			    }, 300);
			  	
	   		}
	   		return false;
	   	}	
	});
	
	Ti.App.addEventListener('searchResultsFound', function(e) {
		var tblSearchResults = SearchResults.renderResultsTable();
		MenuView.viewSearchResults.add(tblSearchResults);
	});	
}

MenuView.switchWindows = function(windowID,data) {
	MenuView.clearWindows();
	
	if (windowID == 'projects') {
		AppInit.detailWindow.animate({view: Projects.win, transition: Styles.windowTransition});
		Ti.App.fireEvent('windowChanged', {
			props: WindowProps.projects
		});
		menuItem1.backgroundImage = 'images/bgMenuItemSelected.png';
		Projects.data.populateProjectTable();
	} else if (windowID == 'tasks') {
		var projectID = 0, projectName = null;
		if (typeof data != 'undefined') {
			projectID = data.projectID;
			projectName = data.winTitle;
		}
		
		AppInit.detailWindow.animate({view: Tasks.win, transition: Styles.windowTransition});
		WindowProps.tasks.title = 'Tasks: '+projectName; 
		Ti.App.fireEvent('windowChanged', {
			props: WindowProps.tasks
		});
		menuItem2.backgroundImage = 'images/bgMenuItemSelected.png';
		Tasks.data.populateTasksTable(projectID);
	} else if (windowID == 'ledger') {
		AppInit.detailWindow.animate({view: Ledger.win, transition: Styles.windowTransition});
		Ti.App.fireEvent('windowChanged', {
			props: WindowProps.ledger
		});
		menuItem3.backgroundImage = 'images/bgMenuItemSelected.png';
	} else if (windowID == 'invoices') {
		AppInit.detailWindow.animate({view: Invoices.win, transition: Styles.windowTransition});
		Ti.App.fireEvent('windowChanged', {
			props: WindowProps.invoices
		});
		menuItem4.backgroundImage = 'images/bgMenuItemSelected.png';
	} else if (windowID == 'expenses') {
		AppInit.detailWindow.animate({view: Expenses.win, transition: Styles.windowTransition});
		Ti.App.fireEvent('windowChanged', {
			props: WindowProps.expenses
		});
		menuItem5.backgroundImage = 'images/bgMenuItemSelected.png';
	} else if (windowID == 'timesheets') {
		AppInit.detailWindow.animate({view: Timesheets.win, transition: Styles.windowTransition});
		Ti.App.fireEvent('windowChanged', {
			props: WindowProps.timesheets
		});
		menuItem6.backgroundImage = 'images/bgMenuItemSelected.png';
	} else if (windowID == 'reports') {
		AppInit.detailWindow.animate({view: Reports.win, transition: Styles.windowTransition});
		Ti.App.fireEvent('windowChanged', {
			props: WindowProps.reports
		});
		menuItem7.backgroundImage = 'images/bgMenuItemSelected.png';
	} else if (windowID == 'dashboard') {
		AppInit.detailWindow.animate({view: Dashboard.win, transition: Styles.windowTransition});
		Ti.App.fireEvent('windowChanged', {
			props: WindowProps.dashboard
		});
		Dashboard.data.getDashboardData();
		menuItem0.backgroundImage = 'images/bgMenuItemSelected.png';
	}
}

MenuView.clearWindows = function() {
	AppInit.detailNav.close(Dashboard.win);
	AppInit.detailNav.close(Projects.win);
	AppInit.detailNav.close(Tasks.win);
	AppInit.detailNav.close(Ledger.win);
	AppInit.detailNav.close(Invoices.win);
	AppInit.detailNav.close(Expenses.win);
	AppInit.detailNav.close(Timesheets.win);
	AppInit.detailNav.close(Reports.win);
	
	menuItem0.backgroundImage = 'images/bgMenuItem.png';
	menuItem1.backgroundImage = 'images/bgMenuItem.png';
	menuItem2.backgroundImage = 'images/bgMenuItem.png';
	menuItem3.backgroundImage = 'images/bgMenuItem.png';
	menuItem4.backgroundImage = 'images/bgMenuItem.png';
	menuItem5.backgroundImage = 'images/bgMenuItem.png';
	menuItem6.backgroundImage = 'images/bgMenuItem.png';
	menuItem7.backgroundImage = 'images/bgMenuItem.png';
}
