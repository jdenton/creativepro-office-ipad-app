DocWindow = {};

DocWindow.init = function() {
	DocWindow.win = Ti.UI.createWindow({
		barColor: Styles.modalToolbarColor
	});
	
	var btnClose = Ti.UI.createButton({
		title: 'Close'
	});
	
	DocWindow.win.setLeftNavButton(btnClose);
	DocWindow.DocViewer = Ti.UI.createWebView({ 
		width: Ti.UI.FILL,
		height: Ti.UI.FILL
	});
	DocWindow.win.add(DocWindow.DocViewer);
	
	btnClose.addEventListener('click', function() {
		DocWindow.win.close();
	});
}
