Reports = {};
Reports.win = Ti.UI.createWindow({
	barColor: '#94b735',
	title: 'Reports',
	backgroundColor: '#eeefe1',
	zIndex: 10
});

Reports.init = function() {
	var lblText = Ti.UI.createLabel({
		text: 'Hi there - I am the Reports window.',
		color: '#000',
		top: 0,
		left: 0
	});
	
	Reports.win.add(lblText);
}