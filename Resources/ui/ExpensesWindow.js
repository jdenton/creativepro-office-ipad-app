Expenses = {};
Expenses.win = Ti.UI.createWindow({
	barColor: '#94b735',
	title: 'Expenses',
	backgroundColor: '#eeefe1',
	zIndex: 10
});

Expenses.init = function() {
	var lblText = Ti.UI.createLabel({
		text: 'Hi there - I am the Expenses window.',
		color: '#000',
		top: 0,
		left: 0
	});
	
	Expenses.win.add(lblText);
}