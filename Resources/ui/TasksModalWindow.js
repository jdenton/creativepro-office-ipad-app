TasksModal = {};
TasksModal.ui = {};
TasksModal.ui.buttons = {};
TasksModal.dataStore = {};
TasksModal.action;
TasksModal.taskID;
TasksModal.created = false;

TasksModal.init = function() {
	if (TasksModal.created == false) {
		TasksModal.win = Ti.UI.createWindow({ 
			barColor: Styles.modalToolbarColor,
			backgroundColor: '#fff'
		});
		
		/*
		 * Create our UI views and buttons.
		 */
		TasksModal.createViewView();
		TasksModal.attachViewEventHandlers();	
		TasksModal.createButtons();
		TasksModal.createEditView();
	}	
	
	if (TasksModal.action == 'edit' || TasksModal.action == 'add') {
		TasksModal.openEditView();
	} else {
		/*
		 * We're viewing a task. Get our task data.
		 */
		DataTasks.getTask(TasksModal.taskID);
		
		Ti.App.addEventListener('taskLoaded', function(e) {
			TasksModal.dataStore = e.taskObj[0];
			TasksModal.openViewView();
			DataMessages.getMessagesForSomething(TasksModal.taskID,'task','taskMessagesLoaded');
			TasksModal.populateView();
		});
	}
	TasksModal.created = true;
}	

TasksModal.createViewView = function() {
	TasksModal.ui.viewView  = Ti.UI.createScrollView({
		contentWidth: Ti.UI.FILL,
		contentHeight: Ti.UI.SIZE,
		showHorizontalScrollIndicator: false,
		top: 0,
		backgroundColor: '#fff',
		layout: 'vertical'
	});
	
	TasksModal.ui.viewTop = Ti.UI.createView({
		top: 0,
		width: Ti.UI.FILL,
		height: 68,
		backgroundColor: '#ebf4fd'
	});
	
	TasksModal.ui.lblCheckbox = Ti.UI.createLabel({
		width: 48,
		height: 48,
		borderRadius: 8,
		borderWidth: 1,
		left: 10,
		top: 10,
		checked: true,
		id: 'taskCheckbox',
		borderColor: '#c5c4b5',
		backgroundGradient: Styles.gradient('ltGray')
	});

	TasksModal.ui.lblEnteredBy = Ti.UI.createLabel({
		width: 350,
		height: 20,
		left: 70,
		top: 10,
		font: Styles.type('h3')
	});
	
	TasksModal.ui.lblStatusText = Ti.UI.createLabel({
		top: 36,
		left: 160,
		width: 150,
		height: 20,
		font: Styles.type('subText'),
		color: '#666'
	});		
	
	TasksModal.ui.lblDateDue = Ti.UI.createLabel({
		font: Styles.type('bigText'),
		color: '#7ebcf7',
		textAlign: 'right',
		top: 3,
		left: 320,
		width: 200,
		height: 40,
		shadowColor: '#fff',
		shadowOffset: {x:1, y:1}
	});
	
	TasksModal.ui.lblPriorityIcon = Ti.UI.createLabel({
		width: 14,
		height: 14,
		left: 430
	});
			
	TasksModal.ui.lblPriorityText = Ti.UI.createLabel({
		font: Styles.type('badge'),
		color: '#000',
		width: 100,
		height: 14,
		left: 450
	});
	
	TasksModal.ui.lblTitle = Ti.UI.createLabel({
		font: Styles.type('heading'),
		top: 10,
		left: 10,
		width: Ti.UI.FILL
	});

	TasksModal.ui.lblDescription = Ti.UI.createLabel({
		font: Styles.type('p'),
		top: 10,
		left: 10,
		height: Ti.UI.SIZE
	});
	
	/*
	 * Assigned people list.
	 */
	var barObjAssigned = {
		color: 'blue',
		top: 10,
		left: 10,
		width: 520,
		text: 'Assigned team members'
	}
	var barAssigned = UI.colorBar(barObjAssigned);	
	
	/*
	 * Add files.
	 */
	var barObjFiles = {
		color: 'blue',
		top: 10,
		left: 10,
		width: 520,
		text: 'Files'
	}
	TasksModal.ui.barFiles = UI.colorBar(barObjFiles);
	TasksModal.ui.barFiles.visible = false;

	TasksModal.ui.tblFiles = Ti.UI.createTableView({
		width: 520,
		top: 10,
		rowHeight: 60,
		height: 150,
		visible: false
	});
	
	/*
	 * Comments for this task.
	 */
	var barObjComments = {
		color: 'blue',
		top: 10,
		left: 10,
		width: 520,
		text: 'Comments'
	}
	var barComments = UI.colorBar(barObjComments);
	
	TasksModal.ui.txtComments = Ti.UI.createTextArea({
		value: 'Add comment',
		color: '#bfbdbd',
		font: { fontSize: 17 },
		width: 520,
		height: 50,
		left: 10,
		borderColor: '#ccc',
		borderRadius: 5,
		top: 10,
		keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType: Titanium.UI.RETURNKEY_DONE
	});
	
	TasksModal.ui.tblComments = Ti.UI.createTableView({
		width: 520,
		top: 10,
		rowHeight: 60,
		height: Ti.UI.SIZE
	});
	
	TasksModal.ui.viewTop.add(
		TasksModal.ui.lblCheckbox,
		TasksModal.ui.lblEnteredBy,
		TasksModal.ui.lblDateDue,
		TasksModal.ui.lblStatusText,
		TasksModal.ui.lblPriorityIcon,
		TasksModal.ui.lblPriorityText
	);
	TasksModal.ui.viewView.add(
		TasksModal.ui.viewTop,
		TasksModal.ui.lblTitle,
		TasksModal.ui.lblDescription,
		barAssigned,
		TasksModal.ui.barFiles,
		TasksModal.ui.tblFiles,
		barComments,
		TasksModal.ui.txtComments,
		TasksModal.ui.tblComments
	);
	
	TasksModal.win.add(TasksModal.ui.viewView);
}
	
TasksModal.populateView = function() {
	var t = TasksModal.dataStore;
	TasksModal.ui.lblEnteredBy.text = 'Entered by '+t.CreatorName;
	TasksModal.ui.lblStatusText.text = t.StatusText;
	TasksModal.ui.lblDateDue.text = t.Date_End;
	
	var lblStatus = Utils.getStatusBadge(t.StatusMachine,t.StatusHuman);
	lblStatus.left = 70;
	lblStatus.top = 37;
	lblStatus.id = 'status';
	lblStatus.width = 80;
	TasksModal.ui.viewTop.add(lblStatus);
	
	var priIcon = Utils.getPriorityIcon(t.Priority);
	TasksModal.ui.lblPriorityIcon.backgroundImage = 'images/'+priIcon;
	TasksModal.ui.lblPriorityText.text = t.PriorityText+' priority';
	TasksModal.ui.lblTitle.text = Utils.txtToField(t.Title);
	TasksModal.ui.lblDescription.text =  Utils.txtToField(t.Description);
	
	if (t.Date_End == '') {
		TasksModal.ui.lblPriorityIcon.top = 17;
		TasksModal.ui.lblPriorityText.top = 17;
	} else {
		TasksModal.ui.lblPriorityIcon.top = 46;
		TasksModal.ui.lblPriorityText.top = 46;
	}
	
	if (TasksModal.dataStore.Files.length > 0) {
		TasksModal.renderFiles();
	} else {
		TasksModal.ui.barFiles.visible = false;
		TasksModal.ui.tblFiles.visible = false;
		TasksModal.ui.tblFiles.height = 0;
	}
}	
	
TasksModal.createEditView = function() {
	TasksModal.viewEdit = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		top: 0
	});
	
	var lbl = Ti.UI.createLabel({
		text: 'edit mode',
		top: 0,
		width: Ti.UI.FILL,
		height: 30
	});
		
	TasksModal.viewEdit.add(lbl);
	
	TasksModal.win.add(TasksModal.viewEdit);
}

TasksModal.renderFiles = function() {
	TasksModal.ui.barFiles.visible = true;
	TasksModal.ui.tblFiles.visible = true;
	TasksModal.ui.tblFiles.height = 150;
	
	var files = TasksModal.dataStore.Files;
	var data = [];
	for(a=0;a<=files.length-1;a++) {
		var f = files[a];
		
		var lblIcon = Ti.UI.createImageView({
			left: 0,
			top: 3,
			width: 36,
			height: 36,
			image: 'images/mime_icons/'+f.iconBig,
			borderRadius: 4		
		});
			
		var lblUploader = Ti.UI.createLabel({
			top: 3,
			height: 15,
			left: 46,
			font: Styles.type('subText'),
			text: 'by '+f.UploaderName+' on '+f.DateEntry,
			color: '#999'
		});
		
		var lblFileName = Ti.UI.createLabel({
			width: Ti.UI.FILL,
			height: Ti.UI.SIZE,
			left: 46,
			top: 20,
			text: f.FileNameActual,
			font: Styles.type('p')
		});
			
		data[a] = Ti.UI.createTableViewRow({ 
			backgroundColor: 'white',
			hasChild: false,
			height: 50,
			className: 'file',
			docURL: f.fileLink,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		data[a].add(lblIcon,lblFileName,lblUploader);
	}
	TasksModal.ui.tblFiles.data = data;
}	

TasksModal.renderMessages = function() {
	var messages = TasksModal.dataStore.Messages;
	var data = [];
	for(a=0;a<=messages.length-1;a++) {
		var m = messages[a];
		
		var lblAvatar = Ti.UI.createImageView({
			left: 0,
			top: 3,
			width: 36,
			height: 36,
			image: m.Avatar,
			borderRadius: 4		
		});
			
		var lblNameDate = Ti.UI.createLabel({
			top: 3,
			height: 15,
			left: 46,
			font: Styles.type('subText'),
			text: 'by '+m.FromName+' on '+m.DateSent,
			color: '#999'
		});
		
		var lblMessage = Ti.UI.createLabel({
			width: Ti.UI.FILL,
			height: Ti.UI.SIZE,
			left: 46,
			top: 20,
			text: Utils.txtToField(m.Message)+'\r\n',
			font: Styles.type('p')
		});
			
		data[a] = Ti.UI.createTableViewRow({ 
			backgroundColor: 'white',
			hasChild: false,
			height: Ti.UI.SIZE,
			className: 'message',
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		data[a].add(lblAvatar,lblNameDate,lblMessage);
	}
	TasksModal.ui.tblComments.data = data;
}

TasksModal.attachViewEventHandlers = function() {			
	TasksModal.ui.txtComments.addEventListener('focus', function() {
		this.color = '#000';
		if (this.value == 'Add comment') {
			this.value = '';	
		}
	});
	
	TasksModal.ui.txtComments.addEventListener('return', function() {
		if (this.value != '') {
			var data = {
				toIDs:      '',
				toType:     '',
				toLanguage: '',
				message:    this.value,
				itemType:   'task',
				itemID:     TasksModal.taskID,
				fromType:   Globals.USER_TYPE
			}
			DataMessages.saveMessage(data,'taskMessageSaved');
			this.value = '';
			this.blur();
		}
	});
	
	Ti.App.addEventListener('taskMessagesLoaded', function(e) {
		if (e.messageObj.Messages) {
			TasksModal.dataStore.Messages = e.messageObj.Messages;
			TasksModal.renderMessages();
		} else {
			TasksModal.ui.tblComments.data = [];
		}
	});
	
	Ti.App.addEventListener('taskMessageSaved', function(e) {
		var b=1;
		if (!TasksModal.dataStore.Messages.length) {
			TasksModal.dataStore.Messages = e.messageObj.Messages;
		} else {
			var messagesMaxIndex = TasksModal.dataStore.Messages.length-1;
			
			var newMessageArray = [];
			newMessageArray[0] = e.messageObj.Messages[0];
			for(var a=0;a<=messagesMaxIndex;a++) {
				newMessageArray[b] = TasksModal.dataStore.Messages[a];
				b++;
			}
			TasksModal.dataStore.Messages = newMessageArray;
		}
		TasksModal.renderMessages();
	});
	
	TasksModal.ui.tblFiles.addEventListener('click', function(e) {
		var row = e.row;
		Ti.App.fireEvent('openDocument', {
			docURL: row.docURL	
		});
	});
}

TasksModal.openViewView = function() {
	TasksModal.win.title = 'Task: '+Utils.txtToField(TasksModal.dataStore.Title);
	TasksModal.win.setLeftNavButton(TasksModal.ui.buttons.btnEdit);
	TasksModal.win.setRightNavButton(TasksModal.ui.buttons.btnDone);
	
	TasksModal.ui.viewView.show();
	TasksModal.viewEdit.hide();
}

TasksModal.openEditView = function() {
	if (TasksModal.action == 'add') {
		var winTitle = 'Add new task';
	} else {
		var winTitle = 'Edit '+TasksModal.dataStore.Title;
	}
	
	TasksModal.win.title = winTitle;
	TasksModal.win.setLeftNavButton();
	TasksModal.win.setRightNavButton(TasksModal.ui.buttons.btnDone);
	
	TasksModal.viewView.hide();
	TasksModal.viewEdit.show();
}	

TasksModal.createButtons = function() {
	TasksModal.ui.buttons.btnDone = Ti.UI.createButton({
		title: 'Done',
		style: Ti.UI.iPhone.SystemButtonStyle.DONE
	});
	
	TasksModal.ui.buttons.btnDone.addEventListener('click', function() {
		if (TasksModal.action == 'editFromView') {
			TasksModal.action = 'view';
			TasksModal.win.title = TasksModal.dataStore.Title;
			TasksModal.win.setLeftNavButton(TasksModal.ui.buttons.btnEdit);
			
			TasksModal.viewView.show();
			TasksModal.viewEdit.hide();	
		} else {
			TasksModal.win.close();
		}
	});
	
	TasksModal.ui.buttons.btnEdit = Ti.UI.createButton({
		title: 'Edit',
		image: 'images/icon-bar-top-edit.png',
		style: Titanium.UI.iPhone.SystemButtonStyle.BORDERED
	});
	TasksModal.ui.buttons.btnEdit.addEventListener('click', function() {
		TasksModal.action = 'editFromView';
		TasksModal.openEditView();
	});
}
