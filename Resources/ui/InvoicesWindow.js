Invoices = {};
Invoices.win = Ti.UI.createWindow({
	barColor: '#94b735',
	title: 'Invoices',
	backgroundColor: '#eeefe1',
	zIndex: 10
});

Invoices.init = function() {
	var lblText = Ti.UI.createLabel({
		text: 'Hi there - I am the invoices window.',
		color: '#000',
		top: 0,
		left: 0
	});
	
	Invoices.win.add(lblText);
}