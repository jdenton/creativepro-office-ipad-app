var DataTasks = {}

DataTasks.getTasksForSelect = function(projectID,selectObj,selectedTaskID) {
	var pickerObj = selectObj.pickerObj;
	var fieldButton = selectObj.fieldButton;
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Utils.removeAllPickerRows(pickerObj);
			var msObj,taskObj,msLabel,msRow;
			var pickerData = [], selectedPickerRow;
			if (jdata.length < 1 || jdata.length == null || jdata.length == '') {
				fieldButton.visible = false;
			} else {
				for (var msIndex = 0; msIndex < (jdata.length); msIndex++ ) {
					msObj = jdata[msIndex];
					msLabel = Ti.UI.createLabel({
						left: 0,
						text: msObj.Title,
						font:{fontSize: 21,fontWeight: 'bold'},
						color: '#666'
					});	
					msRow = Ti.UI.createPickerRow({value: '00'});
					msRow.add(msLabel);
					pickerData.push(msRow);
					
					/*
					 * Pick up tasks here
					 */
					for (var taskIndex = 0; taskIndex < (msObj.Tasks.length); taskIndex++ ) {
						taskObj = msObj.Tasks[taskIndex];
						pickerData.push(Ti.UI.createPickerRow({title: '    '+taskObj.Title, value: taskObj.TaskID}));
						
						if (taskObj.TaskID == selectedTaskID) {
							selectedPickerRow = taskIndex;
						}
					}				
				}
				fieldButton.visible = true;
				pickerObj.add(pickerData);
				pickerObj.selectionIndicator = true;
				
				if (typeof selectedTaskID != 'undefined') {
					Timer.ui.projectPicker.setSelectedRow(0,selectedPickerRow+1,false);
				}
			}	
		}	
	}	
	AJAX.open('POST',Globals.SITE_URL+'tasks/TaskView/getTasksForSelect/0/json');
	AJAX.send({
		'authKey'  : Globals.AUTH_KEY,
		'projectID': projectID
	});	 
}

DataTasks.getTasks = function(projectID) {
	Ti.App.fireEvent('showIndicator',{title: 'Getting tasks...'});
	var AJAX = Titanium.Network.createHTTPClient();
		
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Tasks.dataStore = jdata;
			Tasks.loaded = true;
			Ti.App.fireEvent('tasksLoaded');
			Ti.App.fireEvent('hideIndicator');
		}	
	}
		
	if (projectID > 0) {	
		/*
		 * Get tasks for this project
		 */
		Ti.API.info(Globals.SITE_URL+'tasks/TaskView/getTasksForProject/'+projectID+'/json/0');
		AJAX.open('POST',Globals.SITE_URL+'tasks/TaskView/getTasksForProject/'+projectID+'/json/0');
	} else {
		/*
		 * Get default task list (dashboard view)
		 */
		AJAX.open('POST',Globals.SITE_URL+'tasks/TaskView/getTaskDigestView/json/0');
	}
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});		
}

DataTasks.getTask = function(taskID) {
	Ti.App.fireEvent('showIndicator',{title: 'Getting task...'});
	var AJAX = Titanium.Network.createHTTPClient();
		
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Ti.App.fireEvent('hideIndicator');
			Ti.App.fireEvent('taskLoaded', {
				taskObj: jdata	
			});
		}	
	}
	
	AJAX.open('POST',Globals.SITE_URL+'tasks/TaskView/getTask/'+taskID);
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});		
}

DataTasks.getTaskDigestView = function() {
	Ti.App.fireEvent('showIndicator',{title: 'Getting tasks...'});
	var AJAX = Titanium.Network.createHTTPClient();
		
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			Tasks.dataStore = jdata;
			Tasks.loaded = true;
			//Ti.API.info(jdata);
			Ti.App.fireEvent('hideIndicator');
			Ti.App.fireEvent('taskDigestLoaded', {
				taskObj: jdata,
				digestView: true
			});
		}	
	}
	
	AJAX.open('POST',Globals.SITE_URL+'tasks/TaskView/getTaskDigestView/json');
	AJAX.send({
		'authKey': Globals.AUTH_KEY
	});		
}

DataTasks.saveTask = function(data) {
	Ti.App.fireEvent('showIndicator',{title: 'Saving task...'});
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		Ti.App.fireEvent('hideIndicator');
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			DataTasks.getTasks(jdata.ProjectID);
		}	
	}	
	AJAX.open('POST',Globals.SITE_URL+'tasks/TaskUpdate/saveTask');
	AJAX.send({
		'authKey'     : Globals.AUTH_KEY,
		'save'        : 1,
		'action'      : data.action,
		'taskID'      : data.taskID,
		'milestoneID' : data.milestoneID,
		'projectID'   : data.projectID,
		'taskTitle'   : data.taskTitle,
		'taskEstimatedTime' : data.estimatedTime,
		'taskDateStart' : data.taskDateStart,
		'taskDateEnd'   : data.taskDateEnd,
		'priority'      : data.priority,
		'taskTags'      : data.taskTags,
		'description'   : data.description
	});	 
}

DataTasks.updateTaskStatus = function(taskID,newStatus) {
	var AJAX = Titanium.Network.createHTTPClient();
	AJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			
		}	
	}	
		
	AJAX.open('POST', Globals.SITE_URL+'tasks/TaskUpdate/updateTaskStatus/'+taskID+'/'+newStatus);
	AJAX.send({
		'authKey'  : Globals.AUTH_KEY
	});	 
}
