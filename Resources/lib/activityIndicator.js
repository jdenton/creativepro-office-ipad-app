var indWin = null;
var actWin = null;
var actInd;
var indView;
var indicatorShowing = false;
function showIndicator(title) {
    indicatorShowing = true;
 
    // window container
    indWin = Titanium.UI.createWindow({
        backgroundColor:'transparent',
        height:480,
        width:320,
        zIndex: 0
    });
 
    // black view
    indView = Titanium.UI.createView({
        height: 70,
        width: 250,
        backgroundColor: '#000',
        borderRadius: 10,
        opacity: 0.7
    });
 
    actInd = Titanium.UI.createActivityIndicator({
        style: Titanium.UI.iPhone.ActivityIndicatorStyle.BIG,
        font: {
            fontFamily: 'Helvetica Neue',
            fontSize: 22,
            fontWeight: 'bold'
        },
        color: 'white',
        message: title,
        width: 250
    });
 
    indView.add(actInd);
    indWin.add(indView);
 
    actInd.show();
    indWin.open();
}
 
function hideIndicator() {
    if(indicatorShowing == true) {
        indView.animate({
	    	height: 0,
	    	width: 0,
	    	bottom: 0,
	    	duration: 300
	    }, function() {
	    	if (actInd) {
	            actInd.hide();
	        }
	        if(indWin != null) {
	            indWin.close();
	            indWin = null
	        }
	        indicatorShowing = false;
	    });
    }
}

Titanium.App.addEventListener('showIndicator', function(e) {
    if(e.title == null) {
        e.title = 'Loading... ';
    }
    if(indicatorShowing == true) {
        hideIndicator();
    }
    showIndicator(e.title);
});
Titanium.App.addEventListener('hideIndicator', function(e) {
    hideIndicator();
});