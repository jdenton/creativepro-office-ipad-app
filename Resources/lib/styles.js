Styles = {}

Styles.navBarColor       = '#94b735';
Styles.appBGColor        = '#eeefe1';
Styles.toolbarColor      = '#4396be';
Styles.modalToolbarColor = '#8cc5f2';

/*
 * POPOVER PICKER ANIMATIONS
 */
Styles.poPickerSlideUp   = Ti.UI.createAnimation({bottom: 0});
Styles.poPickerSlideDown = Ti.UI.createAnimation({bottom: -260});

Styles.gradient = function(color) {
	var colorStart, colorEnd;
	switch(color) {
		case 'green':
			colorStart = '#c6ec54';
			colorEnd   = '#9acc01';
			break;
		case 'red':
			colorStart = '#faa3a2';
			colorEnd   = '#f21413';
			break;
		case 'ltGray':
			colorStart = '#f5f5f5';
			colorEnd   = '#e2dede';
			break;
		case 'mdGray':
			colorStart = '#f2f2f2';
			colorEnd   = '#ccc9c9';
			break;
		case 'dkGray':
			colorStart = '#464c4d';
			colorEnd   = '#252627';
			break;
		case 'ltBlue':
			colorStart = '#ebf4fd';
			colorEnd   = '#7ebcf7';
			break;
		case 'mdBlue':
			colorStart = '#81a2dd';
			colorEnd   = '#193476';
			break;	
		default:
			colorStart = '#6b81c0';
			colorEnd   = '#35509e';
			break;
	}
	
	var style = {
		type: 'linear',
		startPoint: { x: '0%', y: '0%' },
		endPoint: { x: '0%', y: '100%' },
		colors: [{ color: colorStart, offset: 0.0},{color: colorEnd, ofset: 0.5}]
	}
	
	return style;
}

Styles.menuLabel = function(lblText,lblIcon,top,id) {
	var view = Ti.UI.createView({
		top: top,
		left: 0,
		width: 300,
		height: 50,
		id: id,
		backgroundImage: 'images/bgMenuItem.png'
	});
	
	var label = Ti.UI.createLabel({
		text: lblText,
		font: { 
	    	fontSize: 20,
	    	fontWeight: 'bold',
	    	fontFamily: 'franklin'
	    },
	    width: 240,
	    left: 60,
	    color: 'white',
	    shadowColor: 'black',
	    shadowOffset:{ x: 1, y: 1 }
	});
	
	var icon = Ti.UI.createLabel({
		width: 30,
		height: 30,
		left: 10,
		backgroundImage: 'images/'+lblIcon
	});
	
	view.add(icon,label);
	
	return view;
}

Styles.itemTypeIcons = function(itemType) {
	var icon = '';
	switch(itemType) {
		case 'invoice':
			icon = 'invoice.png';
			break;
		case 'project':
			icon = 'projects.png';
			break;
		case 'expense':
			icon = 'expenses.png';
			break;	
		case 'task':
			icon = 'tasks.png';
			break;
		case 'ledger':
			icon = 'ledger.png';
			break;	
		case 'client':
			icon = 'user.png';
			break;			
	}
	
	return icon;
}

Styles.navBarButton = function(title,color) {
	var borderColor, txtColor;
	switch(color) {
		case 'mdGray':
			borderColor = '#ccc9c9';
			txtColor = '#a3a1a1';
			break;
		case 'ltBlue':
			borderColor = '#3787d3';
			txtColor = '#2873ba';
			break;	
	}
	
	var style = {
		borderColor: borderColor,	    
	    color: txtColor,
	    backgroundImage: 'none',
	    borderRadius: 5,
	    borderWidth: 1,
	    height: 30,
	    font:{ fontSize: 13 },
	    width: 50,
	    title: title,
	    backgroundGradient: globals.styles.gradient(color)
	}
	return style;    
}

Styles.tableViewBubble = function() {
	var style = {
		backgroundColor: '#8fa2c1',
		borderColor: '#8fa2c1',
		borderRadius: 11,
		left: 240,
		height: 23,
		width: 30,
		color: '#fff',
		textAlign: 'center',
		font:{ fontSize: 18, fontWeight: 'bold' },
		shadowColor: '#758bad',
	    shadowOffset:{ x:1, y:1}
	}
	return style;
}

Styles.badge = function(text,color,update,lblBadge) {
	switch(color) {
		case 'red':
			backgroundColor = '#FEC3D2';
			txtColor = '#000';
			shadowColor = '#fff';
			break;
		case 'blue':
			backgroundColor = '#DAEFFE';
			txtColor = '#000';
			shadowColor = '#FFF';
			break;
		case 'green':
			backgroundColor = '#B2DD32';
			txtColor = '#000';
			shadowColor = '#FFF';
			break;
		case 'dk-green':
			backgroundColor = '#86b108';
			txtColor = '#000';
			shadowColor = '#FFF';
			break;	
		case 'yellow':
			backgroundColor = '#E0E01F';
			txtColor = '#000';
			shadowColor = '#FFF';
			break;	
		case 'gray':
			backgroundColor = '#ccc';
			txtColor = '#000';
			shadowColor = '#FFF';
			break;
		case 'purple':
			backgroundColor = '#b349e4';
			txtColor = '#000';
			shadowColor = '#FFF';
			break;	
		case 'orange':
			backgroundColor = '#f7bc68';
			txtColor = '#000';
			shadowColor = '#FFF';
			break;								
	}
	
	if (update == true) {
		lblBadge.text = '   '+text+'   ';
		lblBadge.backgroundColor = backgroundColor;
	} else {	
		var lblBadge = Ti.UI.createLabel({
			height: 17,
			text: '   '+text+'   ',
			backgroundColor: backgroundColor,
			color: txtColor,
		    font: Styles.type('badge'),
		    borderRadius: 8,
		    textAlign: 'center'
		});
		
		return lblBadge;
	}	
}

Styles.tableSectionHeader = function(height) {
	if (height == '') {
		height = 40;
	}
	var sectionHeader = Ti.UI.createView({ 
		top: 0,
		height: height,
		width: Ti.UI.FILL, 
		backgroundColor: '#ebf4fd'
	});
	var sectionHeaderBorder = Ti.UI.createLabel({
		height: 1,
		width: Ti.UI.FILL,
		top: parseInt(height-1),
		backgroundColor: '#85b6e6'
	});
	sectionHeader.add(sectionHeaderBorder);
	return sectionHeader;
}

Styles.type = function(className) {
	var type = {};
	switch(className) {
		case 'h1':
			type = {fontFamily: 'Helvetica-Bold', fontSize: 18};
			break;
		case 'heading':
			type = {fontFamily: 'Helvetica-Light', fontSize: 24};
			break;	
		case 'h2':
			type = {fontFamily: 'Helvetica-Bold', fontSize: 18};
			break;
		case 'h3':
			type = {fontFamily: 'Helvetica', fontSize: 15};
			break;	
		case 'subText':
			type = {fontFamily: 'Helvetica-Oblique', fontSize: 13};
			break;
		case 'badge':
			type = {fontFamily: 'Helvetica', fontSize: 12};
			break;
		case 'bigText':
			type = {fontFamily: 'Helvetica-Bold', fontSize: 32};
			break;
		case 'p':
			type = {fontFamily: 'Helvetica', fontSize: 15};
			break;	
		case 'ltBlue':
			break;
		default:
			break;
	}
	return type;
}

Styles.windowTransition = Ti.UI.iOS.ANIMATION_CURVE_EASE_IN;
