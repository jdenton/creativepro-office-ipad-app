WindowProps = {};

WindowProps.projects = {
	title: 'Projects',
	id: 'projects'
}

WindowProps.tasks = {
	title: 'Tasks',
	id: 'tasks'
}

WindowProps.ledger = {
	title: 'Ledger',
	id: 'ledger'
}

WindowProps.invoices = {
	title: 'Invoices',
	id: 'invoices'
}

WindowProps.expenses = {
	title: 'Expenses',
	id: 'expenses'
}

WindowProps.timesheets = {
	title: 'Timesheets',
	id: 'timesheets'
}

WindowProps.reports = {
	title: 'Reports',
	id: 'reports'
}

WindowProps.dashboard = {
	title: 'Dashboard',
	id: 'dashboard'
}

WindowProps.getButtonBar = function(windowID) {
	var btnBar = Ti.UI.createView();
	WindowProps.btnBack = Ti.UI.createButton({
		width: 34,
		height: 34,
		backgroundImage: 'images/icon-arrow-left.png'
	});	
	WindowProps.btnAdd = Ti.UI.createButton({
		width: 34,
		height: 34,
		backgroundImage: 'images/icon-bar-bottom-add.png'
	});
	
	if (windowID == 'projects') {
		btnBar.width = 100;
		WindowProps.btnAdd.right = 0;
		WindowProps.btnBack.right = 45;
		
		WindowProps.btnAdd.addEventListener('click', function() {
			ProjectsModal.action = 'add';			
			ProjectsModal.init();
			ProjectsModal.win.open({
				modal: true,
				modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
			});
		});
		
		btnBar.add(WindowProps.btnBack,WindowProps.btnAdd);
	} else if (windowID == 'tasks') {
		btnBar.width = 100;
		WindowProps.btnAdd.right = 0;
		WindowProps.btnBack.right = 45;
		
		WindowProps.btnAdd.addEventListener('click', function() {
			Ti.App.fireEvent('openModal', {
				values: ['task','add',null,null]
			});
		});
		
		btnBar.add(WindowProps.btnBack,WindowProps.btnAdd);
	} else if (windowID == 'dashboard') {
		
	}
	
	WindowProps.btnBack.addEventListener('click', function() {
		MenuView.switchWindows(Globals.historyWin);
	});
	
	return btnBar;
}	

/*
 * Some event listeners for controlling windows.
 */
Ti.App.addEventListener('windowChanged', function(e) {
	var winProps = e.props;
	Globals.historyWin = Globals.currentWin;
	Globals.currentWin = winProps.id;
	
	AppInit.detailWindow.title = winProps.title;
	AppInit.detailWindow.rightNavButton = WindowProps.getButtonBar(winProps.id);
});	

Ti.App.addEventListener('openDocument', function(e) {
	
	DocWindow.DocViewer.url = e.docURL;
	DocWindow.win.open({ modal: true });
});	

Ti.App.addEventListener('openModal', function(e) {
		var windowType = e.values[0];
		var action = e.values[1];
		var itemID = e.values[2];
		var itemObj = e.values[3];
		var modalStyle = Ti.UI.iPhone.MODAL_PRESENTATION_FORMSHEET;
		var modal;
		switch(windowType) {
			case 'project':
				modal = ProjectsModal;
				modal.dataStore = itemObj;
				modal.action = action;
				modal.projectID = itemID;
				modal.init();
				modal.win.open({
					modal: true,
					modalStyle: modalStyle
				});
				break;
			case 'task':
				modal = TasksModal;
				modal.dataStore = itemObj;
				modal.action = action;
				modal.taskID = itemID;
				modal.init();
				modal.win.open({
					modal: true,
					modalStyle: modalStyle
				});
				break;	
			case 'message':
				modal = MessageModal;
				modal.dataStore = itemObj;
				modal.action = action;
				modal.taskID = itemID;
				modal.init();
				modal.win.open({
					modal: true,
					modalStyle: modalStyle
				});
				break;		
		}		
	});	
